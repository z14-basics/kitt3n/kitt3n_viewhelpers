<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\Date;


/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class CombineViewHelper extends AbstractViewHelper {


    /**
     * initialize arguments
     *
     * DateTime :: dStart
     * DateTime :: dEnd
     *
     * String :: sFormat
     * String :: sFormatStartDiffDay
     * String :: sFormatStartDiffDayMonth
     * String :: sFormatStartDiffDayMonthYear
     * String :: sFormatEndtDiffDay
     * String :: sFormatEndDiffDayMonth
     * String :: sFormatEndDiffDayMonthYear
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('dStart', 'date', 'Start date', true);
        $this->registerArgument('dEnd', 'date', 'End date', true);

        $this->registerArgument('sFormat', 'string', 'Default format of the dates', true);
        $this->registerArgument('sFormatStartDiffDay', 'string', '...', false);
        $this->registerArgument('sFormatStartDiffDayMonth', 'string', '...', false);
        $this->registerArgument('sFormatStartDiffDayMonthYear', 'string', '...', false);
        $this->registerArgument('sFormatEndtDiffDay', 'string', '...', false);
        $this->registerArgument('sFormatEndDiffDayMonth', 'string', '...', false);
        $this->registerArgument('sFormatEndDiffDayMonthYear', 'string', '...', false);

    }


    /**
     *
     * @return mixed
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N/Kitt3nViewhelpers/ViewHelpers}
     * ...
     * <v:variable.set name="startDate" value="{f:format.date(date: '17.01.1979', format: 'd.m.Y')}" />
     * <v:variable.set name="endDate" value="{f:format.date(date: '19.01.1979', format: 'd.m.Y')}" />
     *
     * <kitt3n:date.combine dStart="{startDate}" dEnd="{endDate}" sFormat="%d.%m.%Y" />
     * <kitt3n:date.combine dStart="{startDate}" dEnd="{endDate}" sFormat="%d.%m.%Y" sFormatStartDiffDay="%d." sFormatStartDiffDayMonth="%d.%m." sFormatStartDiffDayMonthYear="%d.%m.%Y" sFormatEndtDiffDay="%d.%m.%Y" sFormatEndDiffDayMonth="%d.%m.%Y" sFormatEndDiffDayMonthYear="%d.%m.%Y" />
     * ...
     *
     */
    public function render() {
        $dStart = $this->arguments['dStart'];
        $dEnd = $this->arguments['dEnd'];

        $sFormat = $this->arguments['sFormat'];
        $sFormatStartDiffDay = $this->arguments['sFormatStartDiffDay'];
        $sFormatStartDiffDayMonth = $this->arguments['sFormatStartDiffDayMonth'];
        $sFormatStartDiffDayMonthYear = $this->arguments['sFormatStartDiffDayMonthYear'];
        $sFormatEndtDiffDay = $this->arguments['sFormatEndtDiffDay'];
        $sFormatEndDiffDayMonth = $this->arguments['sFormatEndDiffDayMonth'];
        $sFormatEndDiffDayMonthYear = $this->arguments['sFormatEndDiffDayMonthYear'];


        // if dates are not in date format > cast them
        if(gettype($dStart) != 'object'){
            $dStart = new \DateTime($dStart);
        }
        if(gettype($dEnd) != 'object'){
            $dEnd = new \DateTime($dEnd);
        }

        $aCompareDates = [
            'start' => [
                'day' => intval(
                    $dStart->format('d')
                ),
                'month' => intval(
                    $dStart->format('m')
                ),
                'year' => intval(
                    $dStart->format('Y')
                )
            ],
            'end' => [
                'day' => intval(
                    $dEnd->format('d')
                ),
                'month' => intval(
                    $dEnd->format('m')
                ),
                'year' => intval(
                    $dEnd->format('Y')
                )
            ],
            'format' => $sFormat
            // %m/%d/%Y | %d.%m.%Y
        ];


        if(
            $aCompareDates['start']['day'] &&
            $aCompareDates['start']['month'] &&
            $aCompareDates['start']['year'] &&
            $aCompareDates['end']['day'] &&
            $aCompareDates['end']['month'] &&
            $aCompareDates['end']['year'] &&
            $aCompareDates['format']){

            if($aCompareDates['start']['year'] == $aCompareDates['end']['year']){

                if($aCompareDates['start']['month'] == $aCompareDates['end']['month']){

                    if($aCompareDates['start']['day'] == $aCompareDates['end']['day']){

                        // all 3 components of both dates are equal
                        $sReturn = $this->formatDate(
                            $aCompareDates['format'],
                            $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                        );
                        return $sReturn;

                    } else {
                        // the 2 last components of both dates are equal but the day is different
                        if($aCompareDates['start']['day'] < $aCompareDates['end']['day']){

                            // start date string (cropped)
                            $sStart = $this->formatDate(
                                ($sFormatStartDiffDay ? $sFormatStartDiffDay : $sFormat),
                                $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                            );

                            // end date string
                            $sEnd = $this->formatDate(
                                ($sFormatEndtDiffDay ? $sFormatEndtDiffDay : $sFormat),
                                $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                            );

                            $sReturn = $sStart .' - '. $sEnd;
                            return $sReturn;

                        } else {
                            // ERROR :: invalid order...start-date is bigger than end-date
                            return FALSE;
                        }
                    }
                } else {
                    // the last component of both dates are equal but the day and month are different
                    if($aCompareDates['start']['day'] < $aCompareDates['end']['day']){

                        // start date string (cropped)
                        $sStart = $this->formatDate(
                            ($sFormatStartDiffDayMonth ? $sFormatStartDiffDayMonth : $sFormat),
                            $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                        );

                        // end date string
                        $sEnd = $this->formatDate(
                            ($sFormatEndDiffDayMonth ? $sFormatEndDiffDayMonth : $sFormat),
                            $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                        );

                        $sReturn = $sStart .' - '. $sEnd;
                        return $sReturn;

                    } else {
                        // ERROR :: invalid range...start-date is bigger than end-date
                        return FALSE;
                    }
                }
            } else {
                if($aCompareDates['start']['year'] < $aCompareDates['end']['year']){

                    $sStart = $this->formatDate(
                        ($sFormatStartDiffDayMonthYear ? $sFormatStartDiffDayMonthYear : $sFormat),
                        $aCompareDates['start']['day'] .'-'. $aCompareDates['start']['month'] . '-' . $aCompareDates['start']['year']
                    );

                    // end date string
                    $sEnd = $this->formatDate(
                        ($sFormatEndDiffDayMonthYear ? $sFormatEndDiffDayMonthYear : $sFormat),
                        $aCompareDates['end']['day'] .'-'. $aCompareDates['end']['month'] . '-' . $aCompareDates['end']['year']
                    );

                    $sReturn = $sStart .' - '. $sEnd;
                    return $sReturn;

                } else {
                    // ERROR :: invalid range...start-date is bigger than end-date
                    return FALSE;
                }
            }
        } else {
            // ERROR :: not all dates are set
            return FALSE;
        }
    }


    /**
     * @param $sFormat
     * @param $sDate
     * @return false|string
     */
    public function formatDate ($sFormat, $sDate){

        $date = new \DateTime($sDate);
        $timestamp = $date->getTimestamp();
        if (strpos($sFormat,"%") !== false) {
            return strftime($sFormat, $timestamp);
        }
        return $date->format($sFormat);
    }

}
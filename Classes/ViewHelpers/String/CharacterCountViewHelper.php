<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\String;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class CharacterCountViewHelper extends AbstractViewHelper {

    use CompileWithRenderStatic;

    /**
     * initialize arguments
     *
     * String :: str
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('str', 'string', 'String of which the characters should be counted', true);
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return int|mixed
     *
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N/Kitt3nViewhelpers/ViewHelpers}
     * ...
     * <kitten:string.characterCount str="hallo"/>
     * ...
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        return strlen($arguments['str']);
    }
}
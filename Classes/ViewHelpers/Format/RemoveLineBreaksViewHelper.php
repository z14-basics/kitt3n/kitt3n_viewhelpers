<?php
namespace KITT3N\Kitt3nViewhelpers\ViewHelpers\Format;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Oliver Merz, Georg Kathan, Dominik Hilser - kitt3n.de
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Prepares text for use in JavaScript: removes line breaks
 */
class RemoveLineBreaksViewHelper extends AbstractViewHelper {

    /**
     * initialize arguments
     *
     * String :: text
     *
     */
    public function initializeArguments()
    {
        $this->registerArgument('text', 'string', 'Text in which the linebreaks should be replaced', true);
    }

    /**
     *
     * @return string
     *
     * Use e.g.:
     * <html xmlns:kitt3n="http://typo3.org/ns/KITT3N/Kitt3nViewhelpers/ViewHelpers">
     * or
     * {namespace kitt3n=KITT3N\Kitt3nViewhelpers\ViewHelpers}
     * ...
     * <kitt3n:format.removeLineBreaks text="Zeile1 \n Zeile2 \n Zeile 3"/>
     * ...
     */
    public function render()
    {
        if(!$this->arguments['text']) {
            $this->arguments['text'] = $this->renderChildren();
        }
        $text = str_replace(array('\n', '\r'), '', $this->arguments['text']);
        return $text;
    }

}